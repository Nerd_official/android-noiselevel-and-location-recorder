# Android NoiseLevel And Location Recorder

Coursework App spec
• Create a noise logging app in the cloud
• Use microphone to sample sound
• Calculate noise level
• Get location reading (coarse and/or fine grain location)
• Write your sample and location metadata to Firebase
• Include phone ID
• Use Firebase and the Firebase SDK for location logging
• Provide a simple user interface for the app
• Start/stop logging, configure firebase account, configure
frequency of logging, show estimated noise level, etc