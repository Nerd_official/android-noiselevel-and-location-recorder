package com.example.peat.noiselogger.Model;

import java.util.Map;

/**
 * This a model class for noise data
 *
 * @author Wayne Walker
 */

public class Noise {
    private String name;

    private String date;

    private int frequency;

    private String longitude;

    private String latitude;

    private String phoneId;

    private Map sampleData;

    public Noise(String phoneId, String name, String date, int frequency, String longitude, String latitude, Map sampleData) {
        this.phoneId = phoneId;
        this.name = name;
        this.date = date;
        this.frequency = frequency;
        this.longitude = longitude;
        this.latitude = latitude;
        this.sampleData = sampleData;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getFrequency() {
        return this.frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public Map getSampleData() {
        return sampleData;
    }
    public Noise(){

    }
}
