package com.example.peat.noiselogger;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.peat.noiselogger.View.NoiseLoggerStartFragment;

/**
 *
 * This Activity hosts the fragments
 *
 * @author Wayne Walker
 */

public class NoiseLoggingActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noise_logging);
        fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = new NoiseLoggerStartFragment();
            fragmentManager.executePendingTransactions();
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }
}

