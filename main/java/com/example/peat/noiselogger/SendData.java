package com.example.peat.noiselogger;

import com.example.peat.noiselogger.Model.Noise;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class sends noise sample data to Firebase.
 *
 * @author Wayne Walker
 */

public class SendData implements NoiseLoggerInterface.SendFirebaseData {
    /** Firebase database reference */
    private DatabaseReference mDatabaseRef;

    @Override
    public void onSendRequest(Noise noise, List<Integer> sampleData) {
        // Get logged noise level averages
        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i < sampleData.size(); i++) {
            map.put("average" + i, sampleData.get(i));
        }
        Date date = new Date();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("NoiseSample");
        mDatabaseRef.child(date.toString()).setValue(new Noise(noise.getPhoneId(), noise.getName(), noise.getDate(), noise.getFrequency(), noise.getLongitude(), noise.getLatitude(), map));
    }

}
