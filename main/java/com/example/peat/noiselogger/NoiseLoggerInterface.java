package com.example.peat.noiselogger;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Chronometer;
import android.widget.TextView;

import com.example.peat.noiselogger.Model.Noise;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Interface for that allows communication between View fragments and presenters.
 *
 * @author Wayne Walker
 */

public interface NoiseLoggerInterface {
    interface StartView {
        /**
         * Changes the current fragment to another fragment
         * data is passed to the next fragment via  fragment arguments
         */
        void switchFragment(int time);

        /*
        * Save data to firebase - displays a dialog box
        * */
        void saveToFirebase();

        /*
        *  Set visibility for the set frequency menu buttons
        * */
        void setVisibility(int visibility);

        /*
        *  Display the longitude and latitude
        * */
        void setLocation(double longitude_data, double latitude_data);

        /*
        *  Set cardview visiblilty and display text
        * */
        void setDataFromBundle(int value, int frequency, Date newDate);
    }

    interface nextStartFragment {
        // Calls the presenter when a set frequency menu button is clicked
        void onStartButtonClick(int time);

        // When save button is clicked display a dialog box
        void callAlertDialog();

        // When set frequency button is pressed, hides button
        void onSetVisibility(int visibility);

        // Calls the presenter to get current location
        void onGetLocation(Activity activity, Noise noise);

        // Calls the presenter to get bundle
        void onPassDataFromBundle(int value, int frequency, Noise noise);
    }

    interface nextStopFragment {
        // Call to presenter to intitialize media recorder
        void onMediaButtonClick();

        // Call to presenter to get the amplitude and logs the average after a set interval
        void onGetAmp(TextView date, Chronometer mChronometer);

        /**
         * Call to presenter to change the current fragment to another fragment
         * data is passed to the next fragment via  fragment arguments
         */
        void onSwitchFragment(int time, int frequencyValue, ArrayList sampleData);
    }

    interface StopView {
        // Intitialize media recorder
        void MediaRecorderReady();

        // Get the amplitude and logs the average after a set interval
        void getAmp(TextView date, Chronometer mChronometer);

        // Changes the current fragment to another fragment
        void switchFragment(Bundle args);
    }

    interface SendFirebaseData {
        // Send noise sample data to Firebase
        void onSendRequest(Noise noise, List<Integer> sampleData);
    }
}

