package com.example.peat.noiselogger.Presenter;

import android.os.Bundle;
import android.widget.Chronometer;
import android.widget.TextView;

import com.example.peat.noiselogger.NoiseLoggerInterface;

import java.util.ArrayList;

/**
 * This presenter communicates with NoiseLoggerStopFragment.
 *
 * @author Wayne Walker
 */

public class NoiseLoggerStopPresenter implements NoiseLoggerInterface.nextStopFragment{
    NoiseLoggerInterface.StopView mNoiseView ;

    public NoiseLoggerStopPresenter(NoiseLoggerInterface.StopView mNoiseView) {
        this.mNoiseView = mNoiseView;
    }

    @Override
    public void onMediaButtonClick() {
        this.mNoiseView.MediaRecorderReady();

    }

    @Override
    public void onGetAmp(TextView date, Chronometer mChronometer) {
        this.mNoiseView.getAmp(date, mChronometer);
    }


    @Override
    public void onSwitchFragment(int time, int frequencyValue, ArrayList noiseSampleData) {
        // Put sample data in Bundle
        Bundle args = new Bundle();
        args.putInt("overall_noise_average", time);
        args.putInt("frequency", frequencyValue);
        args.putIntegerArrayList("noiseList", (ArrayList<Integer>) noiseSampleData);
        this.mNoiseView.switchFragment(args);
    }
}
