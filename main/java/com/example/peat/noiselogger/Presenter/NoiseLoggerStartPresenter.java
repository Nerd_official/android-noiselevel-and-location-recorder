package com.example.peat.noiselogger.Presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.peat.noiselogger.Model.Noise;
import com.example.peat.noiselogger.NoiseLoggerInterface;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Date;

/**
 * This presenter communicates with NoiseLoggerStopFragment.
 *
 * @author Wayne Walker
 */

public class NoiseLoggerStartPresenter implements NoiseLoggerInterface.nextStartFragment{
    NoiseLoggerInterface.StartView mNoiseView ;
    private GoogleApiClient mClient;
    double latitude_data;
    double longitude_data;
    NoiseLoggerInterface.SendFirebaseData mNoiseFireView ;

    public NoiseLoggerStartPresenter(NoiseLoggerInterface.StartView mNoiseView) {
        this.mNoiseView = mNoiseView;
    }

    @Override
    public void onStartButtonClick(int time) {
        this.mNoiseView.switchFragment(time);
    }

    @Override
    public void callAlertDialog() {
        this.mNoiseView.saveToFirebase();
    }

    @Override
    public void onSetVisibility(int visibility) {
        this.mNoiseView.setVisibility(visibility);
    }

    @Override
    public void onGetLocation(Activity activity, final Noise noise) {
        /*
        * Get location data if permission has been granted
        *
        * Code taken from the lab location locatr code
        * */
        mClient = new GoogleApiClient.Builder(activity).addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        LocationRequest request = LocationRequest.create();
                        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        request.setNumUpdates(1);
                        request.setInterval(0);
                        LocationServices.FusedLocationApi
                                .requestLocationUpdates(mClient, request, new LocationListener() {
                                    @Override
                                    public void onLocationChanged(Location location) {
                                        latitude_data = location.getLatitude();
                                        longitude_data = location.getLongitude();
                                        mNoiseView.setLocation(longitude_data, latitude_data);
                                        // Save location in noise object
                                        noise.setLongitude(String.valueOf(longitude_data));
                                        noise.setLatitude(String.valueOf(latitude_data));

                                    }
                                });
                    }
                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .build();
        mClient.connect();
    }

    @Override
    public void onPassDataFromBundle(int value, int frequency, Noise noise) {
        Date newDate = new Date();
        //Set noise object data
        noise.setDate(String.valueOf(newDate).substring(0, 10));
        noise.setFrequency(frequency);
        this.mNoiseView.setDataFromBundle(value, frequency, newDate);

    }

}
