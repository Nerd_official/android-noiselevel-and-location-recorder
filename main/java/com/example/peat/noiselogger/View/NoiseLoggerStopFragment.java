package com.example.peat.noiselogger.View;

import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peat.noiselogger.NoiseLoggerInterface;
import com.example.peat.noiselogger.Presenter.NoiseLoggerStopPresenter;
import com.example.peat.noiselogger.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This fragment displays a microphone and the results of the noise level recordings.
 *  When the microphone is pressed, the logged results are sent to the next fragment.
 *
 * @author Wayne Walker
 */

public class NoiseLoggerStopFragment  extends Fragment implements  NoiseLoggerInterface.StopView
{
    NoiseLoggerInterface.nextStopFragment nextStopFragment;
    int total = 0;
    Chronometer mChronometer;
    MediaRecorder mediaRecorder;
    static Boolean checkPlay = true;
    private Thread thread;
    ArrayList<Integer> noiseSampleData = new ArrayList<>();
    int sampleDateAverage = 0;
    int count = 0;
    int overallCount = 0;
    static int frequencyValue = 0;
    TextView date;
    TextView noiseAverage;
    int noiseLogginTotal = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stop, container, false);
        //Get frequency value from fragment argument
        frequencyValue = getArguments().getInt("setFrequency");
        mChronometer = (Chronometer) v.findViewById(R.id.stopFragmentChronometer); // initiate chronometer
        noiseAverage = (TextView) v.findViewById(R.id.show_avg);
        nextStopFragment = new NoiseLoggerStopPresenter(this);
        ImageView microphoneStop = (ImageView) v.findViewById(R.id.imageview);
        microphoneStop.setImageResource(R.drawable.rec_devices);
        // Intialize media recorder
        nextStopFragment.onMediaButtonClick();

        // Start a new thread to run every second
        thread = new Thread(new Runnable() {
            public void run() {
                while (thread != null && !thread.isInterrupted() && checkPlay != false) {
                    try {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    date = (TextView) getView().findViewById(R.id.show_text);
                                    // Start the chronometer
                                    mChronometer.start();
                                    nextStopFragment.onGetAmp(date, mChronometer);
                                }
                            });
                        }
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }
        });
        thread.start();
        // Microphone is pressed to stop recording
        microphoneStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    checkPlay = false;
                    mChronometer.stop();
                }
                // Get the overall average time
                if (overallCount > 0) {
                    sampleDateAverage = total / overallCount;
                }
                // Switch fragments
                nextStopFragment.onSwitchFragment(sampleDateAverage, frequencyValue, noiseSampleData);
            }
        });
        return v;
    }

           @Override
           public void MediaRecorderReady() {
               //Taken from  -> https://www.tutorialspoint.com/android/android_audio_capture.htm
               mediaRecorder=new MediaRecorder();
               mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
               mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
               mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
               mediaRecorder.setOutputFile("dev/null");
               try {
                   mediaRecorder.prepare();
                   mediaRecorder.start();
                   checkPlay = true;
               } catch (IllegalStateException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
               } catch (IOException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
               }
           }

           @Override
           public void getAmp(final TextView noiseLevel, Chronometer mChronometer) {
               // Taken from -> http://abhiandroid.com/ui/chronometer
               mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                   @Override
                   public void onChronometerTick(Chronometer chronometer) {
                       double amp = mediaRecorder.getMaxAmplitude();
                       // Set the noise level in card view
                       noiseLevel.setText(String.valueOf(amp));
                       total += amp;
                       noiseLogginTotal += amp;
                       if (chronometer != null) {
                           count++;
                           overallCount++;
                           // Add average noise level to array list
                           if (count == frequencyValue) {
                               noiseSampleData.add(noiseLogginTotal / count);
                               noiseAverage.setText(String.valueOf(noiseLogginTotal / count));
                               System.out.println("chronometer + " + count);
                               System.out.println("chronometer frequency + " + frequencyValue);
                               System.out.println(" frequency total" + noiseLogginTotal / count);
                               noiseLogginTotal = 0;
                               count = 0;
                           }
                       }
                   }
               });
           }

           @Override
           public void switchFragment(Bundle args) {
               NoiseLoggerStartFragment playFragment = new NoiseLoggerStartFragment();
               playFragment.setArguments(args);
               FragmentManager fManager = getFragmentManager();
               FragmentTransaction fragmentTransaction = fManager.beginTransaction();
               fragmentTransaction.replace(R.id.fragment_container, playFragment);
               //fragmentTransaction.addToBackStack(null);
               fragmentTransaction.commit();
           }
}

