package com.example.peat.noiselogger.View;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.peat.noiselogger.Model.Noise;
import com.example.peat.noiselogger.NoiseLoggerInterface;
import com.example.peat.noiselogger.Presenter.NoiseLoggerStartPresenter;
import com.example.peat.noiselogger.R;
import com.example.peat.noiselogger.SendData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.RECORD_AUDIO;

/**
 * This fragment displays a microphone to start recording. When the microphone is pressed
 * User sets the frequency using the menu
 * After the recording has finished results are displayed
 * The user is given the option to save the data.
 *
 * @author Wayne Walker
 */

public class NoiseLoggerStartFragment extends Fragment implements NoiseLoggerInterface.StartView {
    NoiseLoggerInterface.nextStartFragment nextStartRecFragment;
    CardView cardView;
    List<Integer> noiseSampleData;
    Button saveFirebase;
    Noise noise = new Noise();
    ArrayList<ImageView> mImageView = new ArrayList<>();
    ImageView imageViewLeft;
    ImageView imageViewMiddleLeft;
    ImageView imageViewMiddleRight;
    ImageView imageViewRight;
    TextView longitude;
    TextView latitude;
    TextView overallNoiseAverage;
    TextView fullDate;
    /*
    *
    * Code taken from the lab location locatr code
    * */
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO,
    };
    private static final int REQUEST_PERMISSIONS = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_play, container, false);
        cardView = (CardView) v.findViewById(R.id.card_view);
        cardView.setVisibility(View.INVISIBLE);
        nextStartRecFragment = new NoiseLoggerStartPresenter(this);

        // Find frequency menu
        imageViewLeft = (ImageView) v.findViewById(R.id.imageview_left);
        imageViewMiddleLeft = (ImageView) v.findViewById(R.id.imageview_middle_left);
        imageViewMiddleRight = (ImageView) v.findViewById(R.id.imageview_middle_right);
        imageViewRight = (ImageView) v.findViewById(R.id.imageview_right);
        // Microphone image -> https://www.google.co.uk/search?q=recording+icon&tbm=isch&tbs=rimg:CUD3gZ9p9KP1IjgZDDQ4Xq5U2mlvRi4BJezz-SWnikkLOMmrYQdn5qRn3b4tddbiwTWSXpb3ydqF5BUKnSoZMzz8rioSCRkMNDherlTaEUZA7OIdEM-lKhIJaW9GLgEl7PMRE_1t2TnA_1Q64qEgn5JaeKSQs4yREMX02ClpFQZSoSCathB2fmpGfdEWISLjouyz6FKhIJvi111uLBNZIRAJ0_1Ra6Q50EqEglelvfJ2oXkFRGQPgp5zbbOBSoSCQqdKhkzPPyuEYGUbWRAHy4j&tbo=u&sa=X&ved=2ahUKEwjN7bnDhszZAhXMA8AKHXavB-QQ9C96BAgAEBk&biw=1366&bih=613&dpr=1#imgrc=tRa9KeXvkgN-0M:
        ToggleButton microphoneImage = (ToggleButton) v.findViewById(R.id.imageview_orange_rec);

        // Initialize textviews
        longitude = (TextView) v.findViewById(R.id.longitude_number);
        latitude = (TextView) v.findViewById(R.id.lat_number);
        overallNoiseAverage = (TextView) v.findViewById(R.id.average_number);
        fullDate = (TextView) v.findViewById(R.id.full_date);
        // Firebase save button
        saveFirebase = (Button) v.findViewById(R.id.save_firebase);

        // Set frequency menu button images
        imageViewLeft.setImageResource(R.drawable.five_second);
        imageViewRight.setImageResource(R.drawable.sixty_second);
        imageViewMiddleLeft.setImageResource(R.drawable.ten_second);
        imageViewMiddleRight.setImageResource(R.drawable.thirty_seconds);
        // Set frequency menu visibility
        nextStartRecFragment.onSetVisibility(View.INVISIBLE);

        // Check if fragment has switched and then get data
        Bundle arguments = getArguments();
        if (arguments != null) {
            //Get average noise level from fragment argument
            int value = getArguments().getInt("overall_noise_average");
            int frequency = getArguments().getInt("frequency");
            //Get array list data from fragment argument
            noiseSampleData = getArguments().getIntegerArrayList("noiseList");
            nextStartRecFragment.onPassDataFromBundle(value, frequency, noise);
        }
       // Check if permissions have been granted
       if (checkPermission()) {

       } else {
           requestPermissions(PERMISSIONS,
                   REQUEST_PERMISSIONS);
        }
        // Called when microphone is pressed
        microphoneImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // When microphone is pressed, set visibility for frequency menu
                if(isChecked){
                    nextStartRecFragment.onSetVisibility(View.VISIBLE);
                }else{
                    nextStartRecFragment.onSetVisibility(View.INVISIBLE);
                }
            }
        });

        // Call the presenter when a frequency menu item is pressed
        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { nextStartRecFragment.onStartButtonClick(5); }
        });
        imageViewMiddleLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStartRecFragment.onStartButtonClick(10);
            }
        });
        imageViewMiddleRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStartRecFragment.onStartButtonClick(30);
            }
        });
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStartRecFragment.onStartButtonClick(60);
            }
        });
        //Firebase save button pressed
        saveFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStartRecFragment.callAlertDialog();
            }
        });

       return v;
    }

    @Override
    public void setLocation(double longitude_data, double latitude_data) {
        longitude.setText(String.valueOf(longitude_data));
        latitude.setText(String.valueOf(latitude_data));
    }

    @Override
    public void setDataFromBundle(int value, int frequency, Date newDate) {
        cardView.setVisibility(View.VISIBLE);
        // Set the overall noise average in textview
        overallNoiseAverage.setText(String.valueOf(value));
        // Set the date in textview
        fullDate.setText(String.valueOf(newDate).substring(0, 10));
        nextStartRecFragment.onGetLocation(getActivity(), noise);
    }

    @Override
    public void setVisibility(int visibility) {
        mImageView.add(imageViewLeft);
        mImageView.add(imageViewMiddleLeft);
        mImageView.add(imageViewMiddleRight);
        mImageView.add(imageViewRight);

        for (ImageView item: mImageView) {
            item.setVisibility(visibility);
        }
        mImageView.clear();
    }

    /*
    * Check Permissions for location and recording audio
    * */
    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(),
                ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getActivity(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        /*
        *
        * Code taken from the lab location locatr code
        * */
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length> 0) {
                    boolean LocatePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (!RecordPermission && !LocatePermission) {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();
                        getActivity().onBackPressed();
                    } else if (!RecordPermission) {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();
                        getActivity().onBackPressed();
                    }
                }
                break;
        }
    }

    @Override
    public void switchFragment(int frequencyValue) {
        Bundle args = new Bundle();
        //Put frequency value in a bundle
        args.putInt("setFrequency", frequencyValue);
        //Switch fragment
        NoiseLoggerStopFragment stopFragment = new NoiseLoggerStopFragment();
        stopFragment.setArguments(args);
        FragmentManager fManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, stopFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void saveToFirebase() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View subView = inflater.inflate(R.layout.save_firebase_alert, null);
        final EditText dialogResult = (EditText)subView.findViewById(R.id.alert_firebase);
        /* Get phone id
         * https://stackoverflow.com/questions/16869482/how-to-get-unique-device-hardware-id-in-android
         */
        String android_id = Settings.Secure.getString(this.getActivity().getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        noise.setPhoneId(android_id );
        // Show alert dialog
        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(subView);
        alertDialogBuilder.setTitle("Save to Firebase");
        alertDialogBuilder.setPositiveButton("save",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        cardView.setVisibility(View.INVISIBLE);
                        Toast.makeText(getContext(), "Saved " + dialogResult.getText(), Toast.LENGTH_LONG).show();
                        noise.setName(dialogResult.getText().toString());
                        // Send data to Firebase
                        NoiseLoggerInterface.SendFirebaseData sendData = new SendData();
                        sendData.onSendRequest(noise, noiseSampleData);
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
